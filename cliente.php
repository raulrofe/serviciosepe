<?php

require_once 'config.php';

try {

	ini_set("soap.wsdl_cache_enabled", "0");

	$arraySoap = array(
		'soap_version' => SOAP_1_1,
		'trace'=> 1,
		'encoding'=> 'utf-8',
	);

    $clientSOAP = new SoapClient(URL_WSDL, $arraySoap);

    $result = $clientSOAP->obtenerDatosCentro();

    var_dump($result);

} catch (SoapFault $e) {
    echo '<pre>';
        print_r($e);
    echo '</pre>';
}