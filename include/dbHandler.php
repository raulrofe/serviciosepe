<?php

class DbHandler {

    private $conn;

    function __construct() {
        $this->conn = mysqli_connect("db681212786.db.1and1.com", "dbo681212786", "HQbyyPqm", "db681212786");
        /* Comprueba la conexión */
        if (mysqli_connect_errno()){
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
    }

    public function lastId() {
    	return $this->conn->insert_id;
    }
	
    public function authenticate() {
		$arrayClean=['SOAP-ENV:', 'SOAP:','soap-env:', 'soap:','soapenv:','wsse:','wsu:','impl:'];
		$hdr = file_get_contents("php://input");
		$hdr='<?xml version="1.0" encoding="utf-8"?>'.$hdr;
		$clean_xml = str_ireplace($arrayClean, '', $hdr);
		$xml = simplexml_load_string($clean_xml);
		
		$usernametoken = (string)$xml->Header->Security->UsernameToken->attributes()->Id ;
		$user = (string)$xml->Header->Security->UsernameToken->Username;
		$password = (string)$xml->Header->Security->UsernameToken->Password;
		$nonce = (string) $xml->Header->Security->UsernameToken->Nonce;
		$created = (string)$xml->Header->Security->UsernameToken->Created;
		
		$sql = "SELECT * FROM sepe_usuarios WHERE user='".$user."'";
		$result = mysqli_query($this->conn, $sql);
		if($result->num_rows > 0){
			$user=mysqli_fetch_array($result);
			$passwordToCheck = base64_encode(sha1(base64_decode($nonce) . $created . $user['password'], true));
			if($password == $passwordTocheck){
				return true;
			}else{
				return false;
			}
//			return array(
//				'usernametoken'=>$usernametoken,
//				'nonce'=>$nonce,
//				'created'=>$created,
//				'user'=>$user,
//				'password'=>$password,
//				'passwordToCheck'=> $passwordToCheck,
//			);
		}
    }

    // Obtener centro
    public function obtenerDatosCentro() {
        //Se consulta a la tabla CENTRO y solo recogerá la primera tupla, ya que no debemos tener más centros en nuestra tabla
		$sql = "SELECT * FROM sepe_centro LIMIT 1";
		$result = mysqli_query($this->conn, $sql);

		// Si no existe resultado devolvemos nulo
		if($result->num_rows === 0){
			$log = array(
				'CODIGO_RETORNO' => -1,
				'ETIQUETA_ERROR' => "Error de parametro o etiqueta en los datos de entrada",
			);
		}else{
			// Devolvemos el resultado de la base de datos como array asociativo
			if ($res = mysqli_fetch_assoc($result)) {
				$log = array(
					'CODIGO_RETORNO' => 0,
					'ETIQUETA_ERROR' => "Correcto",
					'RES'			 => $res		
				);
			}
		}

		return $log;
    }

	// Crear centro
    public function crearCentro($xml) {

    	$log = array();

        $origenCentro   = $xml->DATOS_IDENTIFICATIVOS->ID_CENTRO->ORIGEN_CENTRO;
        $codigoCentro   = $xml->DATOS_IDENTIFICATIVOS->ID_CENTRO->CODIGO_CENTRO;
        $nombreCentro   = $xml->DATOS_IDENTIFICATIVOS->NOMBRE_CENTRO;
        $urlPlataforma  = $xml->DATOS_IDENTIFICATIVOS->URL_PLATAFORMA;
        $urlSeguimiento = $xml->DATOS_IDENTIFICATIVOS->URL_SEGUIMIENTO;
        $telefono       = $xml->DATOS_IDENTIFICATIVOS->TELEFONO;
        $email          = $xml->DATOS_IDENTIFICATIVOS->EMAIL;
		//Comprobamos si falta algun parámetro en el xml de petición
		if(empty($origenCentro) || empty($codigoCentro) || empty($nombreCentro || empty($urlPlataforma) || empty($urlSeguimiento) || empty($telefono) || empty($email))){
			// Error en parametro de los datos de entrada codigo retorno 2
			$log = array(
				'CODIGO_RETORNO' => 2,
				'ETIQUETA_ERROR' => "Error de parametro o etiqueta en los datos de entrada",
			);
		}
		// Comprobamos primero si existe registro para saber si tenemos que crear o actualizar
		$sql = "SELECT * FROM sepe_centro LIMIT 1";
		$result = mysqli_query($this->conn, $sql);

		// Si no existe el centro, CREAMOS
		if($result->num_rows === 0){
			$sql = "INSERT INTO sepe_centro (origen_centro, codigo_centro, nombre_centro, url_plataforma, url_seguimiento, telefono, email) "
				. "VALUES ('" . $origenCentro . "', '" . $codigoCentro . "','" . $nombreCentro . "', '" . $urlPlataforma . "', '" . $urlSeguimiento . "', '" . $telefono . "', '" . $email . "')";
		}else{ // Si existe el centro, ACTUALIZAMOS
			$sql = "UPDATE sepe_centro SET origen_centro = '" . $origenCentro . "', codigo_centro = '" . $codigoCentro . "', nombre_centro = '" . $nombreCentro . "', url_plataforma = '" . $urlPlataforma . "', url_seguimiento = '" . $urlSeguimiento . "', telefono = '" . $telefono . "', email = '" . $email . "' where 1=1";
		}

		$result = mysqli_query($this->conn, $sql);

		// Si no se ha relizado la consulta SQL (No existe result) devolvemos nulo
		if($result->num_rows === 0){
			// Erro inesperado, codigo retorno -1
			$log = array(
				'CODIGO_RETORNO' => -1,
				'ETIQUETA_ERROR' => "Error inesperado",
			);
		}else{
			// Busco en acciones formativas
		    $consulta= "SELECT * FROM sepe_accion_formativa";
		    $result = mysqli_query($this->conn, $consulta);

		    if($result->num_rows === 0){
		    	// Correcto
				$log = array(
					'CODIGO_RETORNO' => 0,
					'ETIQUETA_ERROR' => "Correcto",
				);
		    } else {
		    	// Centro con acciones formativas, codigo retorno 1
				$log = array(
					'CODIGO_RETORNO' => 1,
					'ETIQUETA_ERROR' => "Centro con acciones formativas",
				);		    	
		    }
		}

		return $log;
    }

    // FUNCIONES ACCIONES FORMATIVAS
    public function obtenerAccionPorCodigo($codigoAccion){
	    $sql = "SELECT * from sepe_accion_formativa WHERE CODIGO_ACCION = '" . $codigoAccion . "'";
	    $accion = mysqli_query($this->conn, $sql);
	    if(mysqli_num_rows($accion) == 0){
			$log = array(
				'CODIGO_RETORNO' => 0,
				'ETIQUETA_ERROR' => "Correcto",
			);
	    } else {
	    	$log = array(
				'CODIGO_RETORNO' 	=> 1, 
				'ETIQUETA_ERROR' 	=> "La acción ya existe",
				'ACCION_FORMATIVA'	=> mysqli_fetch_array($accion)
			);
	    }

	    return $log;
    }

    // FUNCIONES ACCION
    public function obtenerAccion($origenAccion, $codigoAccion){
	    $sql = "SELECT * from sepe_accion_formativa WHERE CODIGO_ACCION = '" . $codigoAccion . "' AND ORIGEN_ACCION = '" . $origenAccion . "'";
	    $accion = mysqli_query($this->conn, $sql);
	    if(mysqli_num_rows($accion) == 0){
			$log = array(
				'CODIGO_RETORNO' => 1,
				'ETIQUETA_ERROR' => "Acción inexistente",
			);
	    } else {
	    	$log = array(
				'CODIGO_RETORNO' 	=> 0, 
				'ETIQUETA_ERROR' 	=> "Correcto",
				'ACCION_FORMATIVA'	=> mysqli_fetch_array($accion)
			);
	    }

	    return $log;
    }

    public function insertarAccion($accion){

    	if(!em)

    	$sql = "INSERT INTO sepe_accion_formativa (
    	ORIGEN_ACCION, 
    	CODIGO_ACCION, 
    	SITUACION, 
    	ORIGEN_ESPECIALIDAD, 
    	AREA_PROFESIONAL, 
    	CODIGO_ESPECIALIDAD, 
    	DURACION, 
    	FECHA_INICIO, 
    	FECHA_FIN, 
    	IND_ITINERARIO_COMPLETO, 
    	TIPO_FINANCIACION, 
    	NUMERO_ASISTENTES, 
    	DENOMINACION_ACCION, 
    	INFORMACION_GENERAL,
    	HORARIOS, 
    	REQUISITOS,
    	CONTACTO_ACCION) 
        	VALUES (
        '" . $accion->ACCION_FORMATIVA->ID_ACCION->ORIGEN_ACCION . "', 
        '" . $accion->ACCION_FORMATIVA->ID_ACCION->CODIGO_ACCION . "', 
        '" . $accion->ACCION_FORMATIVA->SITUACION . "',
        '" . $accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->ORIGEN_ESPECIALIDAD . "', 
        '" . $accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->AREA_PROFESIONAL . "', 
        '" . $accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->CODIGO_ESPECIALIDAD . "', 
        '" . $accion->ACCION_FORMATIVA->DURACION . "',
        '" . $accion->ACCION_FORMATIVA->FECHA_INICIO . "',
        '" . $accion->ACCION_FORMATIVA->FECHA_FIN . "',
        '" . $accion->ACCION_FORMATIVA->IND_ITINERARIO_COMPLETO . "',
        '" . $accion->ACCION_FORMATIVA->TIPO_FINANCIACION . "',
        '" . $accion->ACCION_FORMATIVA->NUMERO_ASISTENTES . "',
        '" . $accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->DENOMINACION_ACCION . "',
        '" . $accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->INFORMACION_GENERAL . "',
        '" . $accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->HORARIOS . "',
        '" . $accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->REQUISITOS . "',
        '" . $accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->CONTACTO_ACCION . "')";

        return mysqli_query($this->conn, $sql);
    }

    public function insertEspecialidad($especialidad, $idEspecialidad, $centroImparticion, $duracion, $usoEspecialidad, $id){
		$sql = "INSERT INTO sepe_especialidad (
			ORIGEN_ESPECIALIDAD, 
			AREA_PROFESIONAL, 
			CODIGO_ESPECIALIDAD, 
			ORIGEN_CENTRO, 
			CODIGO_CENTRO, 
			FECHA_INICIO, 
			FECHA_FIN, 
			MODALIDAD_IMPARTICION, 
			HORAS_PRESENCIAL, 
			HORAS_TELEFORMACION, 
			NUM_PARTICIPANTES_M, 
			NUMERO_ACCESOS_M, 
			DURACION_TOTAL_M, 
			NUM_PARTICIPANTES_T, 
			NUMERO_ACCESOS_T, 
			DURACION_TOTAL_T, 
			NUM_PARTICIPANTES_N, 
			NUMERO_ACCESOS_N, 
			DURACION_TOTAL_N, 
			NUM_PARTICIPANTES_S, 
			NUMERO_ACTIVIDADES_APRENDIZAJE_S, 
			NUMERO_INTENTOS_S, 
			NUMERO_ACTIVIDADES_EVALUACION_S, 
			REF_ACCION) 
			VALUES (
			'" . $idEspecialidad->ORIGEN_ESPECIALIDAD . "',
			'" . $idEspecialidad->AREA_PROFESIONAL . "',
			'" . $idEspecialidad->CODIGO_ESPECIALIDAD . "',
			'" . $centroImparticion->ORIGEN_CENTRO . "',
			'" . $centroImparticion->CODIGO_CENTRO . "',
			'" . $especialidad->FECHA_INICIO . "',
			'" . $especialidad->FECHA_FIN . "',
			'" . $especialidad->MODALIDAD_IMPARTICION . "',
			'" . $duracion->HORAS_PRESENCIAL . "',
			'" . $duracion->HORAS_TELEFORMACION . "',
			'" . $usoEspecialidad->HORARIO_MANANA->NUM_PARTICIPANTES . "',
			'" . $usoEspecialidad->HORARIO_MANANA->NUMERO_ACCESOS . "',
			'" . $usoEspecialidad->HORARIO_MANANA->DURACION_TOTAL . "',
			'" . $usoEspecialidad->HORARIO_TARDE->NUM_PARTICIPANTES . "',
			'" . $usoEspecialidad->HORARIO_TARDE->NUMERO_ACCESOS . "',
			'" . $usoEspecialidad->HORARIO_TARDE->DURACION_TOTAL . "',
			'" . $usoEspecialidad->HORARIO_NOCHE->NUM_PARTICIPANTES . "',
			'" . $usoEspecialidad->HORARIO_NOCHE->NUMERO_ACCESOS . "',
			'" . $usoEspecialidad->HORARIO_NOCHE->DURACION_TOTAL . "',
			'" . $usoEspecialidad->SEGUIMIENTO_EVALUACION->NUM_PARTICIPANTES . "',
			'" . $usoEspecialidad->SEGUIMIENTO_EVALUACION->NUMERO_ACTIVIDADES_APRENDIZAJE . "',
			'" . $usoEspecialidad->SEGUIMIENTO_EVALUACION->NUMERO_INTENTOS . "',
			'" . $usoEspecialidad->SEGUIMIENTO_EVALUACION->NUMERO_ACTIVIDADES_EVALUACION . "',
			'" . $id . "')";

		return mysqli_query($this->conn, $sql); 	
    }                

    public function obtenerEspecialidades($codigoEspecialidad, $id){
        $sql = "SELECT * from sepe_especialidad WHERE CODIGO_ESPECIALIDAD = '" . $codigoEspecialidad . "' AND REF_ACCION = '" . $id . "'";

        return mysqli_query($this->conn, $sql); 
    }

	public function insertCentroPresencial($centroPresencial, $idEspecialidad, $id){
        $sql = "INSERT INTO sepe_centro_presencial (ORIGEN_CENTRO, CODIGO_CENTRO, REF_ESPECIALIDAD, REF_ACCION) VALUES ('" . $centroPresencial->ORIGEN_CENTRO . "','" . $centroPresencial->CODIGO_CENTRO . "','" . $idEspecialidad . "','" . $id . "')";

    	return mysqli_query($this->conn, $sql); 
	}

	public function obtenerCentrosPresenciales($idEspecialidad, $id){
        $sql = "SELECT * FROM sepe_centro_presencial WHERE REF_ESPECIALIDAD = '" . $idEspecialidad . "' AND REF_ACCION = '" . $id . "'";

    	return mysqli_query($this->conn, $sql); 		
	}

	public function insertTutor($tutorFormador, $idEspecialidad, $id){
        $sql = "INSERT INTO sepe_tutor_formador (TIPO_DOCUMENTO,NUM_DOCUMENTO,LETRA_NIF,ACREDITACION_TUTOR,EXPERIENCIA_PROFESIONAL,COMPETENCIA_DOCENTE,EXPERIENCIA_MODALIDAD_TELEFORMACION,FORMACION_MODALIDAD_TELEFORMACION,REF_ESPECIALIDAD,REF_ACCION) VALUES ('" . $tutorFormador->ID_TUTOR->TIPO_DOCUMENTO . "','" . $tutorFormador->ID_TUTOR->NUM_DOCUMENTO . "','" . $tutorFormador->ID_TUTOR->LETRA_NIF . "','" . $tutorFormador->ACREDITACION_TUTOR . "','" . $tutorFormador->EXPERIENCIA_PROFESIONAL . "','" . $tutorFormador->COMPETENCIA_DOCENTE . "','" . $tutorFormador->EXPERIENCIA_MODALIDAD_TELEFORMACION . "','" . $tutorFormador->FORMACION_MODALIDAD_TELEFORMACION . "','" . $idEspecialidad . "','" . $id . "')";

    	return mysqli_query($this->conn, $sql); 
	}

	public function obtenerTutoresFormadores($idEspecialidad, $id){
        $sql = "SELECT * from sepe_tutor_formador WHERE REF_ESPECIALIDAD = '" . $idEspecialidad . "' AND REF_ACCION = '" . $id . "'";

        return mysqli_query($this->conn, $sql); 			
	}

	public function insertParticipante($participante, $id){
        $sql = "INSERT INTO sepe_participante (TIPO_DOCUMENTO,NUM_DOCUMENTO,LETRA_NIF,INDICADOR_COMPETENCIAS_CLAVE,ID_CONTRATO_CFA,CIF_EMPRESA,TIPO_DOCUMENTO_TE,NUM_DOCUMENTO_TE,LETRA_NIF_TE,TIPO_DOCUMENTO_TF,NUM_DOCUMENTO_TF,LETRA_NIF_TF,REF_ACCION) VALUES ('" . $participante->ID_PARTICIPANTE->TIPO_DOCUMENTO . "','" . $participante->ID_PARTICIPANTE->NUM_DOCUMENTO . "','" . $participante->ID_PARTICIPANTE->LETRA_NIF . "','" . $participante->INDICADOR_COMPETENCIAS_CLAVE . "','" . $participante->CONTRATO_FORMACION->ID_CONTRATO_CFA . "','" . $participante->CONTRATO_FORMACION->CIF_EMPRESA . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_EMPRESA->TIPO_DOCUMENTO . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_EMPRESA->NUM_DOCUMENTO . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_EMPRESA->LETRA_NIF . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_FORMACION->TIPO_DOCUMENTO . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_FORMACION->NUM_DOCUMENTO . "','" . $participante->CONTRATO_FORMACION->ID_TUTOR_FORMACION->LETRA_NIF . "','" . $id . "')";

        return mysqli_query($this->conn, $sql); 			
	}

	public function obtenerParticipante($nif, $id){
		$sql = "SELECT * from sepe_participante WHERE NUM_DOCUMENTO = '" . $nif . "' AND REF_ACCION = '" . $id . "'";

       return mysqli_query($this->conn, $sql); 	
	} 

	public function obtenerParticipantesAccion($id){
		$sql = "SELECT * from sepe_participante WHERE REF_ACCION = '" . $id . "'";

       return mysqli_query($this->conn, $sql); 	
	}                    

	public function insertEspecialidadParticipante($especialidadParticipante, $idParticipante, $id){
        $sql = "INSERT INTO sepe_especialidad_participante (
        ORIGEN_ESPECIALIDAD,
        AREA_PROFESIONAL,
        CODIGO_ESPECIALIDAD,
        FECHA_ALTA,
        FECHA_BAJA,
        ORIGEN_CENTRO_EX,
        CODIGO_CENTRO_EX,
        FECHA_INICIO_EX,
        FECHA_FIN_EX,
        RESULTADO_FINAL,
        CALIFICACION_FINAL,
        PUNTUACION_FINAL,
        REF_PARTICIPANTE,
        REF_ACCION) 
        VALUES (
        '" . $especialidadParticipante->ID_ESPECIALIDAD->ORIGEN_ESPECIALIDAD . "',
        '" . $especialidadParticipante->ID_ESPECIALIDAD->AREA_PROFESIONAL . "',
        '" . $especialidadParticipante->ID_ESPECIALIDAD->CODIGO_ESPECIALIDAD . "',
        '" . $especialidadParticipante->FECHA_ALTA . "',
        '" . $especialidadParticipante->FECHA_BAJA . "',
        '" . $especialidadParticipante->EVALUACION_FINAL->CENTRO_PRESENCIAL_EVALUACION->ORIGEN_CENTRO . "',
        '" . $especialidadParticipante->EVALUACION_FINAL->CENTRO_PRESENCIAL_EVALUACION->CODIGO_CENTRO . "',
        '" . $especialidadParticipante->EVALUACION_FINAL->FECHA_INICIO . "',
        '" . $especialidadParticipante->EVALUACION_FINAL->FECHA_FIN . "',
        '" . $especialidadParticipante->RESULTADOS->RESULTADO_FINAL . "',
        '" . $especialidadParticipante->RESULTADOS->CALIFICACION_FINAL . "',
        '" . $especialidadParticipante->RESULTADOS->PUNTUACION_FINAL . "',
        '" . $idParticipante . "',
        '" . $id . "')";

    	return mysqli_query($this->conn, $sql);
	}

	public function insertTutoriasPresenciales($tutoriaPresencial){
        $sql = "INSERT INTO sepe_tutorias_presenciales (
        ORIGEN_CENTRO,
        CODIGO_CENTRO,
        FECHA_INICIO,
        FECHA_FIN) 
        VALUES (
        '". $tutoriaPresencial->CENTRO_PRESENCIAL_TUTORIA->ORIGEN_CENTRO ."',
        '". $tutoriaPresencial->CENTRO_PRESENCIAL_TUTORIA->CODIGO_CENTRO ."',
        '". $tutoriaPresencial->FECHA_INICIO ."',
        '". $tutoriaPresencial->FECHA_FIN ."')";

        return mysqli_query($this->conn, $sql); ;
	}

	public function obtenerEspecialidadParticipante($idParticipante, $id){
        $sql = "SELECT * FROM sepe_especialidad_participante WHERE REF_PARTICIPANTE='" . $idParticipante . "' AND REF_ACCION = '" . $id . "'";

        return mysqli_query($this->conn, $sql);		
	}

	public function obtenerEspecialidadesAccion($id){
		$sql = "SELECT * from sepe_especialidad WHERE REF_ACCION = '".$id."'";
		return mysqli_query($this->conn, $sql);
	}

    // Obtener listado de acciones formativas
    public function obtenerListaAcciones(){
        //Se consulta a la tabla CENTRO y solo recogerá la primera tupla, ya que no debemos tener más centros en nuestra tabla
		$sql = " SELECT * FROM sepe_accion_formativa";
		$result = mysqli_query($this->conn, $sql); 
		return $result;   	
    }

    // Eliminar accion formativa
    public function eliminarAccion($accion){

    	if(!empty($accion->ID_ACCION->ORIGEN_ACCION) AND !empty($accion->ID_ACCION->CODIGO_ACCION)){

		    //Obtengo la accion formativa
		    $sql = "SELECT * from sepe_accion_formativa WHERE ORIGEN_ACCION = '" . $accion->ID_ACCION->ORIGEN_ACCION . "' AND CODIGO_ACCION = '" . $accion->ID_ACCION->CODIGO_ACCION . "'";

		    $res = mysqli_query($this->conn, $sql);

		    $naccion = mysqli_fetch_array($res);
		    $id = $naccion["id"];

		    //Si existe accion formativa
		    if($id > 0){

		        //Eliminar accion formativa
		        $consultaBorrado="DELETE FROM sepe_accion_formativa WHERE sepe_accion_formativa.id = '".$id."'";         
		        $borradoAccion=mysqli_query($this->conn, $consultaBorrado);

		        //Eliminar especialidad
		        $consultaBorrado1="DELETE FROM sepe_especialidad WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion1=mysqli_query($this->conn, $consultaBorrado1);

		        //Eliminar centro presencial
		        $consultaBorrado12="DELETE FROM sepe_centro_presencial WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion12=mysqli_query($this->conn, $consultaBorrado12);  

		        //Elemeinar especialidad centro presencial
		        $consultaBorrado121="DELETE FROM sepe_esp_centro_presencial WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion121=mysqli_query($this->conn, $consultaBorrado121); 
		    
		        //Eliminar tutor formador
		        $consultaBorrado13="DELETE FROM sepe_tutor_formador WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion13=mysqli_query($this->conn, $consultaBorrado13);    

		        //Eliminar participante
		        $consultaBorrado2="DELETE FROM sepe_participante WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion2=mysqli_query($this->conn, $consultaBorrado2);  
		        
		        //Eliminar especialidad del participante
		        $consultaBorrado2="DELETE FROM sepe_especialidad_participante WHERE REF_ACCION = '".$id."'";         
		        $borradoAccion2=mysqli_query($this->conn, $consultaBorrado2);  

				$log = array(
					'CODIGO_RETORNO' => 0,
					'ETIQUETA_ERROR' => "Correcto",
				);

		    } else {
				$log = array(
					'CODIGO_RETORNO' => 1,
					'ETIQUETA_ERROR' => "Accion formativa inexistente",
				);	
		    }
    	} else {
			$log = array(
				'CODIGO_RETORNO' => 2,
				'ETIQUETA_ERROR' => "Error en parámetro de entrada",
			);	    		
    	}



	    return $log;    	
    }
}

?>