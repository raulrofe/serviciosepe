<?php

//Se importa el archivo con las constantes
require_once 'config.php';
require_once 'include/dbHandler.php';

//Se carga la librería soap
if (!extension_loaded("soap")) {
    dl("php_soap.dll");
}

//Deshabilita que se guarde en caché la info del wsdl
ini_set("soap.wsdl_cache_enabled", "0");

//Se crea un objeto SoapServer poniendo la ruta donde esta el archivo .wsdl
//La ruta se recoge de la constante URL_WSDL
$server = new SoapServer(URL_WSDL, array('encoding' => 'UTF-8'));

/**
 * Crea el centro y devuelve los datos del centro
 *
 * @return array 
 */
function crearCentro($centro) {
    //Se crea un objeto de la clase DbHandler, donde hay métodos auxiliares que trabajan con la BD
    $dbHandler  = new DbHandler();
    $authenticate = $dbHandler->authenticate();

    if($authenticate){
        $log = $dbHandler->crearCentro($centro);

        if($log['CODIGO_RETORNO'] == 0){

            $log = $dbHandler->obtenerDatosCentro();
     
            if($log['CODIGO_RETORNO'] == 0){

                return array(
                    'RESPUESTA_DATOS_CENTRO' =>
                        array(
                            'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                            'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'], 
                            'DATOS_IDENTIFICATIVOS' => 
                                array(
                                    'ID_CENTRO' =>
                                        array(
                                            'ORIGEN_CENTRO' => $log['RES']['ORIGEN_CENTRO'], 
                                            'CODIGO_CENTRO' => $log['RES']['CODIGO_CENTRO']
                                        ),
                                    'NOMBRE_CENTRO' => $log['RES']['NOMBRE_CENTRO'],
                                    'URL_PLATAFORMA' => $log['RES']['URL_PLATAFORMA'],
                                    'URL_SEGUIMIENTO' => $log['RES']['URL_SEGUIMIENTO'],
                                    'TELEFONO' => $log['RES']['TELEFONO'],
                                    'EMAIL' => $log['RES']['EMAIL']
                                )
                        )
                    );
            } else {
                return array (
                    'RESPUESTA_DATOS_CENTRO' => 
                        array(
                            'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                            'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'], 
                            'DATOS_IDENTIFICATIVOS' => null
                        )
                );            
            }

        } else {
            return array (
                'RESPUESTA_DATOS_CENTRO' => 
                    array(
                        'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                        'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'], 
                        'DATOS_IDENTIFICATIVOS' => null
                    )
                );
        }
    } else {
        return array (
        'RESPUESTA_DATOS_CENTRO' => 
            array(
                'CODIGO_RETORNO' => -1,
                'ETIQUETA_ERROR' => 'Error inesperado: Error de logeo', 
                'DATOS_IDENTIFICATIVOS' => null
            )
        );        
    }
}

/**
 * Devuelve los datos del centro
 *
 * @return array 
 */
function obtenerDatosCentro() {

	// Se crea un objeto de la clase DbHandler, donde hay métodos auxiliares que trabajan con la BD
    $dbHandler = new DbHandler();
	$authenticate = $dbHandler->authenticate();

    if($authenticate){
        $log=$dbHandler->obtenerDatosCentro();

        if($log['CODIGO_RETORNO'] == 0){

            return array(
                'RESPUESTA_DATOS_CENTRO' =>
                    array(
                        'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                        'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'], 
                        'DATOS_IDENTIFICATIVOS' => 
                            array(
                                'ID_CENTRO' =>
                                    array(
                                        'ORIGEN_CENTRO' => $log['RES']['ORIGEN_CENTRO'], 
                                        'CODIGO_CENTRO' => $log['RES']['CODIGO_CENTRO']
                                    ),
                                'NOMBRE_CENTRO' => $log['RES']['NOMBRE_CENTRO'],
                                'URL_PLATAFORMA' => $log['RES']['URL_PLATAFORMA'],
                                'URL_SEGUIMIENTO' => $log['RES']['URL_SEGUIMIENTO'],
                                'TELEFONO' => $log['RES']['TELEFONO'],
                                'EMAIL' => $log['RES']['EMAIL']
                            )
                    )
                );
        } else {
            return array (
                'RESPUESTA_DATOS_CENTRO' => 
                    array(
                        'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                        'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'],
                        'DATOS_IDENTIFICATIVOS' => null 
                    )
            );            
        }
    } else {
        return array (
            'RESPUESTA_DATOS_CENTRO' => 
                array(
                    'CODIGO_RETORNO' => -1,
                    'ETIQUETA_ERROR' => 'Error inesperado: error de logeo',
                    'DATOS_IDENTIFICATIVOS' => null 
                )
        );        
    }
}


function crearAccion($accion) {

    $dbHandler = new DbHandler();

    $authenticate = $dbHandler->authenticate();

    if($authenticate){

        $list = array();
        $list3 = array();

        if( !empty($accion->ACCION_FORMATIVA->ID_ACCION->ORIGEN_ACCION) AND 
        !empty($accion->ACCION_FORMATIVA->ID_ACCION->CODIGO_ACCION) AND 
        !empty($accion->ACCION_FORMATIVA->SITUACION) AND
        !empty($accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->ORIGEN_ESPECIALIDAD) AND 
        !empty($accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->AREA_PROFESIONAL) AND 
        !empty($accion->ACCION_FORMATIVA->ID_ESPECIALIDAD_PRINCIPAL->CODIGO_ESPECIALIDAD) AND 
        !empty($accion->ACCION_FORMATIVA->DURACION) AND
        !empty($accion->ACCION_FORMATIVA->FECHA_INICIO) AND
        !empty($accion->ACCION_FORMATIVA->FECHA_FIN) AND
        !empty($accion->ACCION_FORMATIVA->IND_ITINERARIO_COMPLETO) AND
        !empty($accion->ACCION_FORMATIVA->TIPO_FINANCIACION) AND
        !empty($accion->ACCION_FORMATIVA->NUMERO_ASISTENTES) AND
        !empty($accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->DENOMINACION_ACCION) AND
        !empty($accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->INFORMACION_GENERAL) AND
        !empty($accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->HORARIOS) AND
        !empty($accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->REQUISITOS) AND
        !empty($accion->ACCION_FORMATIVA->DESCRIPCION_ACCION->CONTACTO_ACCION)) {

            // OBTENER ACCION FORMATIVA POR CODIGO
            $resultadoAccionFormativa = $dbHandler->obtenerAccionPorCodigo($accion->ACCION_FORMATIVA->ID_ACCION->CODIGO_ACCION);

            if ($resultadoAccionFormativa['CODIGO_RETORNO'] == 0) {

                //INSERTAMOS LA ACCION FORMATIVA SI NO EXISTE
                $log = $dbHandler->insertarAccion($accion);

                //VOLVEMOS A BUSCAR LA ACCION FORMATIVA POR CODIGO PARA COMPROBAR QUE EXISTE Y RECOGER EL ID
                $resultadoAccionFormativa = $dbHandler->obtenerAccionPorCodigo($accion->ACCION_FORMATIVA->ID_ACCION->CODIGO_ACCION);


                if ($resultadoAccionFormativa['CODIGO_RETORNO'] == 1){

                    //OBTENGO LOS DATOS Y EL ID
                    $id = $resultadoAccionFormativa['ACCION_FORMATIVA']["id"];

                    //FOREACH EN EL QUE RECORRO LAS ESPECIALIDADES
                    foreach($accion->ACCION_FORMATIVA->ESPECIALIDADES_ACCION as $especialidad){


                        $idEspecialidad     = $especialidad->ID_ESPECIALIDAD;
                        $centroImparticion  = $especialidad->CENTRO_IMPARTICION;
                        $duracion           = $especialidad->DATOS_DURACION;

                        if ($especialidad->USO) {
                            $usoEspecialidad = $especialidad->USO;
                        } else {
                            $usoEspecialidad = null;
                        }

                        //RESULTADO DE ESPECIALIDAD
                        $dbHandler->insertEspecialidad($especialidad, $idEspecialidad, $centroImparticion, $duracion, $usoEspecialidad, $id);
                        
                        $idEspecialidad = $dbHandler->lastId();

                        //INSERTAR LOS VALORES DE LOS CENTROS PRESENCIALES
                        foreach($especialidad->CENTROS_SESIONES_PRESENCIALES as $centroPresencial) {
                            //INSETAR CENTRO PRESENCIAL
                            $dbHandler->insertCentroPresencial($centroPresencial, $idEspecialidad, $id);  
                        }

                        //INSERTAR LOS TUTORES FORMADORES
                        foreach($especialidad->TUTORES_FORMADORES as $tutorFormador) {

                            //INSETAR TUTRO
                            $dbHandler->insertTutor($tutorFormador, $idEspecialidad, $id);
                        }

                        /////////////////////////////////////////////////////////////////////////////*///////////////////////////////////////////////////////

                        $resultadoCentrosPresenciales = array();

                        //Obtengo los centros presenciales
                        $res = $dbHandler->obtenerCentrosPresenciales($idEspecialidad, $id);

                        while ($row1 = mysqli_fetch_assoc($res)) {
                            array_push($resultadoCentrosPresenciales, array(
                                'ORIGEN_CENTRO' => $row1['ORIGEN_CENTRO'],
                                'CODIGO_CENTRO' => $row1['CODIGO_CENTRO']));
                        }

                        $resultadoTutoresFormadores = array();

                        //Obtengo los tutores
                        $res = $dbHandler->obtenerTutoresFormadores($idEspecialidad, $id);

                        while ($row2 = mysqli_fetch_assoc($res)) {
                            array_push($resultadoTutoresFormadores, array(
                                'ID_TUTOR' => array(
                                    'TIPO_DOCUMENTO' => $row2['TIPO_DOCUMENTO'],
                                    'NUM_DOCUMENTO' => $row2['NUM_DOCUMENTO'],
                                    'LETRA_NIF' => $row2['LETRA_NIF']),
                                'ACREDITACION_TUTOR' => $row2['ACREDITACION_TUTOR'],
                                'EXPERIENCIA_PROFESIONAL' => $row2['EXPERIENCIA_PROFESIONAL'],
                                'COMPETENCIA_DOCENTE' => $row2['COMPETENCIA_DOCENTE'],
                                'EXPERIENCIA_MODALIDAD_TELEFORMACION' => $row2['EXPERIENCIA_MODALIDAD_TELEFORMACION'],
                                'FORMACION_MODALIDAD_TELEFORMACION' => $row2['FORMACION_MODALIDAD_TELEFORMACION']));
                        }

                        // OBTENGO LAS ESPECIALIDADES
                        $res = $dbHandler->obtenerEspecialidades($especialidad->ID_ESPECIALIDAD->CODIGO_ESPECIALIDAD, $id);

                        while ($row = mysqli_fetch_assoc($res)) {
                            array_push($list, 
                                array(
                                    'ID_ESPECIALIDAD' => 
                                        array(
                                            'ORIGEN_ESPECIALIDAD'   => $row['ORIGEN_ESPECIALIDAD'],
                                            'AREA_PROFESIONAL'      => $row['AREA_PROFESIONAL'],
                                            'CODIGO_ESPECIALIDAD'   => $row['CODIGO_ESPECIALIDAD']
                                        ),
                                            'CENTRO_IMPARTICION'    => 
                                        array(
                                            'ORIGEN_CENTRO'         => $row['ORIGEN_CENTRO'],
                                            'CODIGO_CENTRO'         => $row['CODIGO_CENTRO']
                                        ),
                                    'FECHA_INICIO'          => $row['FECHA_INICIO'],
                                    'FECHA_FIN'             => $row['FECHA_FIN'],
                                    'MODALIDAD_IMPARTICION' => $row['MODALIDAD_IMPARTICION'],
                                    'DATOS_DURACION' => 
                                        array(
                                            'HORAS_PRESENCIAL'      => $row['HORAS_PRESENCIAL'], 
                                            'HORAS_TELEFORMACION'   => $row['HORAS_TELEFORMACION']
                                        ),
                                    //HAY QUE GENERAR CONSULTAS
                                    'CENTROS_SESIONES_PRESENCIALES' =>  array(
                                        'CENTRO_PRESENCIAL' => $resultadoCentrosPresenciales),
                                        // HAY QUE GENERAR LAS CONSULTAS PARA LOS TUTORES
                                        'TUTORES_FORMADORES' => array('TUTOR_FORMADOR' => $resultadoTutoresFormadores),
                                        'USO' => array(
                                                'HORARIO_MANANA' => 
                                                    array(
                                                        'NUM_PARTICIPANTES' => $row['NUM_PARTICIPANTES_M'],
                                                        'NUMERO_ACCESOS'    => $row['NUMERO_ACCESOS_M'],
                                                        'DURACION_TOTAL'    => $row['DURACION_TOTAL_M']
                                                    ),
                                                'HORARIO_TARDE'     => 
                                                    array(
                                                        'NUM_PARTICIPANTES' => $row['NUM_PARTICIPANTES_T'],
                                                        'NUMERO_ACCESOS'    => $row['NUMERO_ACCESOS_T'],
                                                        'DURACION_TOTAL'    => $row['DURACION_TOTAL_T']
                                                    ),
                                                'HORARIO_NOCHE' => 
                                                    array(
                                                        'NUM_PARTICIPANTES' => $row['NUM_PARTICIPANTES_N'],
                                                        'NUMERO_ACCESOS'    => $row['NUMERO_ACCESOS_N'],
                                                        'DURACION_TOTAL'    => $row['DURACION_TOTAL_N']
                                                    ),
                                                'SEGUIMIENTO_EVALUACION' => 
                                                    array(
                                                        'NUM_PARTICIPANTES' => $row['NUM_PARTICIPANTES_S'],
                                                        'NUMERO_ACTIVIDADES_APRENDIZAJE' => $row['NUMERO_ACTIVIDADES_APRENDIZAJE_S'],
                                                        'NUMERO_INTENTOS' => $row['NUMERO_INTENTOS_S'],
                                                        'NUMERO_ACTIVIDADES_EVALUACION' => $row['NUMERO_ACTIVIDADES_EVALUACION_S']
                                                    )
                                        ),
                                )
                            );
                        }
                    }

                    //  INSERT POR CADA PARTICIPANTE
                    foreach($accion->ACCION_FORMATIVA->PARTICIPANTES as $participante) {


                        $dbHandler->insertParticipante($participante, $id);
                        $idParticipante = $dbHandler->lastId();

                        foreach($participante->ESPECIALIDADES_PARTICIPANTE as $especialidadParticipante) {
                            $idEspecialidadParticipante = $dbHandler->insertEspecialidadParticipante($especialidadParticipante, $idParticipante, $id)->insert_id;

                            foreach ($especialidadParticipante->TUTORIAS_PRESENCIALES->TUTORIA_PRESENCIAL as $tutoriaPresencial) {

                                $idParticipante = $dbHandler->insertTutoriasPresenciales($tutoriaPresencial);
                            }
                        } 
                        
                        // OBTENGO ESPECIALIDAD DE LOS PARTICIPANTES
                        $resultadoEspecialidadesParticipantes = array();

                        $res = $dbHandler->obtenerEspecialidadParticipante($idParticipante, $id);

                        while ($row1 = mysqli_fetch_assoc($res)) {

                            array_push($resultadoEspecialidadesParticipantes, array(
                                'ID_ESPECIALIDAD' => array('ORIGEN_ESPECIALIDAD' => $row1['ORIGEN_ESPECIALIDAD'], 'AREA_PROFESIONAL' => $row1['AREA_PROFESIONAL'], 'CODIGO_ESPECIALIDAD' => $row1['CODIGO_ESPECIALIDAD']),
                                'FECHA_ALTA' => $row1['FECHA_ALTA'],
                                'FECHA_BAJA' => $row1['FECHA_BAJA'],
                                'TUTORIAS_PRESENCIALES' => array(
                                    'TUTORIA_PRESENCIAL' => array(
                                        'CENTRO_PRESENCIAL_TUTORIA' => array('ORIGEN_CENTRO' => $row1['ORIGEN_CENTRO'], 'CODIGO_CENTRO' => $row1['CODIGO_CENTRO']),
                                        'FECHA_INICIO' => $row1['FECHA_INICIO_T'],
                                        'FECHA_FIN' => $row1['FECHA_FIN_T'])),
                                'EVALUACION_FINAL' => array(
                                    'CENTRO_PRESENCIAL_EVALUACION' => array('ORIGEN_CENTRO' => $row1['ORIGEN_CENTRO_EX'], 'CODIGO_CENTRO' => $row1['CODIGO_CENTRO_EX']),
                                    'FECHA_INICIO' => $row1['FECHA_INICIO_EX'],
                                    'FECHA_FIN' => $row1['FECHA_FIN_EX']),
                                'RESULTADOS' => array(
                                    'RESULTADO_FINAL' => $row1['RESULTADO_FINAL'],
                                    'CALIFICACION_FINAL' => $row1['CALIFICACION_FINAL'],
                                    'PUNTUACION_FINAL' => $row1['PUNTUACION_FINAL']))
                            );
                        }

                        $res = $dbHandler->obtenerParticipante($participante->ID_PARTICIPANTE->NUM_DOCUMENTO, $id);

                        while ($row3 = mysqli_fetch_assoc($res)) {

                            array_push($list3, array(
                                'ID_PARTICIPANTE' => array('TIPO_DOCUMENTO' => $row3['TIPO_DOCUMENTO'], 'NUM_DOCUMENTO' => $row3['NUM_DOCUMENTO'], 'LETRA_NIF' => $row3['LETRA_NIF']),
                                'INDICADOR_COMPETENCIAS_CLAVE' => $row3['INDICADOR_COMPETENCIAS_CLAVE'],
                                'CONTRATO_FORMACION' => array(
                                    'ID_CONTRATO_CFA' => $row3['ID_CONTRATO_CFA'],
                                    'CIF_EMPRESA' => $row3['CIF_EMPRESA'],
                                    'ID_TUTOR_EMPRESA' => array('TIPO_DOCUMENTO' => $row3['TIPO_DOCUMENTO_TE'], 'NUM_DOCUMENTO' => $row3['NUM_DOCUMENTO_TE'], 'LETRA_NIF' => $row3['LETRA_NIF_TE']),
                                    'ID_TUTOR_FORMACION' => array('TIPO_DOCUMENTO' => $row3['TIPO_DOCUMENTO_TF'], 'NUM_DOCUMENTO' => $row3['NUM_DOCUMENTO_TF'], 'LETRA_NIF' => $row3['LETRA_NIF_TF'])),
                                'ESPECIALIDADES_PARTICIPANTE' => array('ESPECIALIDAD' => $resultadoEspecialidadesParticipantes)
                            ));
                        }
                    }

                    return array('RESPUESTA_OBT_ACCION' => array(
                            'CODIGO_RETORNO' => 0,
                            'ETIQUETA_ERROR' => "Correcto",
                            'ACCION_FORMATIVA' => array(
                                'ID_ACCION' => array('ORIGEN_ACCION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['ORIGEN_ACCION'], 'CODIGO_ACCION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['CODIGO_ACCION']),
                                'SITUACION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['SITUACION'],
                                'ID_ESPECIALIDAD_PRINCIPAL' => array('ORIGEN_ESPECIALIDAD' => $resultadoAccionFormativa['ACCION_FORMATIVA']['ORIGEN_ESPECIALIDAD'], 'AREA_PROFESIONAL' => $resultadoAccionFormativa['ACCION_FORMATIVA']['AREA_PROFESIONAL'], 'CODIGO_ESPECIALIDAD' => $resultadoAccionFormativa['ACCION_FORMATIVA']['CODIGO_ESPECIALIDAD']),
                                'DURACION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['DURACION'],
                                'FECHA_INICIO' => $resultadoAccionFormativa['ACCION_FORMATIVA']['FECHA_INICIO'],
                                'FECHA_FIN' => $resultadoAccionFormativa['ACCION_FORMATIVA']['FECHA_FIN'],
                                'IND_ITINERARIO_COMPLETO' => $resultadoAccionFormativa['ACCION_FORMATIVA']['IND_ITINERARIO_COMPLETO'],
                                'TIPO_FINANCIACION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['TIPO_FINANCIACION'],
                                'NUMERO_ASISTENTES' => $resultadoAccionFormativa['ACCION_FORMATIVA']['NUMERO_ASISTENTES'],
                                'DESCRIPCION_ACCION' => array('DENOMINACION_ACCION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['DENOMINACION_ACCION'], 'INFORMACION_GENERAL' => $resultadoAccionFormativa['ACCION_FORMATIVA']['INFORMACION_GENERAL'], 'HORARIOS' => $resultadoAccionFormativa['ACCION_FORMATIVA']['HORARIOS'], 'REQUISITOS' => $resultadoAccionFormativa['ACCION_FORMATIVA']['REQUISITOS'], 'CONTACTO_ACCION' => $resultadoAccionFormativa['ACCION_FORMATIVA']['CONTACTO_ACCION']),
                                'ESPECIALIDADES_ACCION' => array('ESPECIALIDAD' => $list),
                                'PARTICIPANTES' => array('PARTICIPANTE' => $list3)
                    )));

                } // FIN DE ACCION FORMATIVA CUANDO EXISTE PARA PROCEDE A INSERTAR LAS DEPENDENCIAS
            } else {
                return array(
                        'RESPUESTA_OBT_ACCION' => 
                            array(
                                'CODIGO_RETORNO' => $resultadoAccionFormativa['CODIGO_RETORNO'],
                                'ETIQUETA_ERROR' => $resultadoAccionFormativa['ETIQUETA_ERROR'],
                                'ACCION_FORMATIVA' => null            
                            )
                );
            }
        } else {
            return array(
                    'RESPUESTA_OBT_ACCION' => 
                        array(
                            'CODIGO_RETORNO' => 2,
                            'ETIQUETA_ERROR' => 'Error en parámetros de entrada',
                            'ACCION_FORMATIVA' => null            
                        )
            );            
        }
    } else {
        return array(
                'RESPUESTA_OBT_ACCION' => 
                    array(
                        'CODIGO_RETORNO' => -1,
                        'ETIQUETA_ERROR' => 'Error inesperado: Error de logeo',
                        'ACCION_FORMATIVA' => null            
                    )
        );         
    }
}

/**
 * Ontengo un listado de las acciones formativas
 *
 * @return array 
 */
function obtenerListaAcciones() {    
    $dbHandler = new DbHandler();

    $authenticate = $dbHandler->authenticate();

    if($authenticate){
        $results = $dbHandler->obtenerListaAcciones();

        $list = array();

        while($row = mysqli_fetch_assoc($results)) {
            array_push($list, array('ORIGEN_ACCION' => $row['ORIGEN_ACCION'], 'CODIGO_ACCION' => $row['CODIGO_ACCION']));
        }

        return array(
                'RESPUESTA_OBT_LISTA_ACCIONES' => 
                    array(
                        'CODIGO_RETORNO' => 0,
                        'ETIQUETA_ERROR' => "Correcto",
                        'ID_ACCION' => $list
                    )
        );

    } else {
        return array(
                'RESPUESTA_OBT_LISTA_ACCIONES' => 
                    array(
                        'CODIGO_RETORNO' => -1,
                        'ETIQUETA_ERROR' => 'Error inesperado: Error de logeo',
                        'ID_ACCION' => null
                    )
        );
    }

 }

/**
 * Obtener Accion formativa
 *
 * @return array 
 */
function obtenerAccion($accion) {

    $dbHandler = new DbHandler();

    $authenticate = $dbHandler->authenticate();

    if($authenticate){

        $accionFormativa = array();
        $log = $dbHandler->obtenerAccion($accion->ID_ACCION->ORIGEN_ACCION, $accion->ID_ACCION->CODIGO_ACCION);

        if(!empty($accion->ID_ACCION->ORIGEN_ACCION) && !empty($accion->ID_ACCION->CODIGO_ACCION)){
            if($log['CODIGO_RETORNO'] == 0){


                $id = $log['ACCION_FORMATIVA']["id"];

                $accionFormativa["ORIGEN_ACCION"]           = $log['ACCION_FORMATIVA']["ORIGEN_ACCION"];
                $accionFormativa["CODIGO_ACCION"]           = $log['ACCION_FORMATIVA']["CODIGO_ACCION"];
                $accionFormativa["SITUACION"]               = $log['ACCION_FORMATIVA']["SITUACION"];
                $accionFormativa["ORIGEN_ESPECIALIDAD"]     = $log['ACCION_FORMATIVA']["ORIGEN_ESPECIALIDAD"];
                $accionFormativa["AREA_PROFESIONAL"]        = $log['ACCION_FORMATIVA']["AREA_PROFESIONAL"];
                $accionFormativa["CODIGO_ESPECIALIDAD"]     = $log['ACCION_FORMATIVA']["CODIGO_ESPECIALIDAD"];
                $accionFormativa["DURACION"]                = $log['ACCION_FORMATIVA']["DURACION"];
                $accionFormativa["FECHA_INICIO"]            = $log['ACCION_FORMATIVA']["FECHA_INICIO"];
                $accionFormativa["FECHA_FIN"]               = $log['ACCION_FORMATIVA']["FECHA_FIN"];
                $accionFormativa["IND_ITINERARIO_COMPLETO"] = $log['ACCION_FORMATIVA']["IND_ITINERARIO_COMPLETO"];
                $accionFormativa["TIPO_FINANCIACION"]       = $log['ACCION_FORMATIVA']["TIPO_FINANCIACION"];
                $accionFormativa["NUMERO_ASISTENTES"]       = $log['ACCION_FORMATIVA']["NUMERO_ASISTENTES"];
                $accionFormativa["DENOMINACION_ACCION"]     = $log['ACCION_FORMATIVA']["DENOMINACION_ACCION"];
                $accionFormativa["INFORMACION_GENERAL"]     = $log['ACCION_FORMATIVA']["INFORMACION_GENERAL"];
                $accionFormativa["HORARIOS"]                = $log['ACCION_FORMATIVA']["HORARIOS"];
                $accionFormativa["REQUISITOS"]              = $log['ACCION_FORMATIVA']["REQUISITOS"];
                $accionFormativa["CONTACTO_ACCION"]         = $log['ACCION_FORMATIVA']["CONTACTO_ACCION"];
                
                $list= array();
                $list1= array();
                $list2= array();
                $list3= array();

                $especialidades = $dbHandler->obtenerEspecialidadesAccion($id);


                while($especialidad = mysqli_fetch_array($especialidades)){


                    //Centros presenciales 
                    $centrosPresenciales = $dbHandler->obtenerCentrosPresenciales($especialidad['ID_ESPECIALIDAD'], $id);
                    while($centroPresencial = mysqli_fetch_array($centrosPresenciales)){       

                        array_push ($list1, array ('ORIGEN_CENTRO' => 20, 'CODIGO_CENTRO' => $centroPresencial['CODIGO_CENTRO'])) ;
                    }

                    //Tutores formadores
                    $tutores = $dbHandler->obtenerTutoresFormadores($especialidad['ID_ESPECIALIDAD'], $id);
                    while ($tutor = mysqli_fetch_assoc($tutores)){
                        array_push($list2, array(
                            'ID_TUTOR' => 
                            array(
                                'TIPO_DOCUMENTO' => $tutor['TIPO_DOCUMENTO'], 
                                'NUM_DOCUMENTO' => $tutor['NUM_DOCUMENTO'], 
                                'LETRA_NIF' => $tutor['LETRA_NIF']
                            ),
                            'ACREDITACION_TUTOR' => $tutor['ACREDITACION_TUTOR'],
                            'EXPERIENCIA_PROFESIONAL' => $tutor['EXPERIENCIA_PROFESIONAL'],
                            'COMPETENCIA_DOCENTE' => $tutor['COMPETENCIA_DOCENTE'],
                            'EXPERIENCIA_MODALIDAD_TELEFORMACION' => $tutor['EXPERIENCIA_MODALIDAD_TELEFORMACION'],
                            'FORMACION_MODALIDAD_TELEFORMACION' => $tutor['FORMACION_MODALIDAD_TELEFORMACION']
                            )
                        );
                    }

                    //
                    $list[]= array(
                                'ID_ESPECIALIDAD' => 
                                    array ( 
                                        'ORIGEN_ESPECIALIDAD' => $especialidad['ORIGEN_ESPECIALIDAD'], 
                                        'AREA_PROFESIONAL' => $especialidad['AREA_PROFESIONAL'], 
                                        'CODIGO_ESPECIALIDAD' => $especialidad['CODIGO_ESPECIALIDAD']
                                    ),
                                'CENTRO_IMPARTICION' => 
                                    array(
                                        'ORIGEN_CENTRO' => $especialidad['ORIGEN_CENTRO'], 
                                        'CODIGO_CENTRO' => $especialidad['CODIGO_CENTRO']
                                    ),
                                'FECHA_INICIO' => $especialidad['FECHA_INICIO'],
                                'FECHA_FIN' => $especialidad['FECHA_FIN'],
                                'MODALIDAD_IMPARTICION' => $especialidad['MODALIDAD_IMPARTICION'],
                                'DATOS_DURACION' => 
                                    array(
                                        'HORAS_PRESENCIAL' => $especialidad['HORAS_PRESENCIAL'], 
                                        'HORAS_TELEFORMACION' => $especialidad['HORAS_TELEFORMACION']
                                    ),

                                //HAY QUE GENERAR CONSULTAS
                                'CENTROS_SESIONES_PRESENCIALES' =>  array('CENTRO_PRESENCIAL'=> $list1),

                                // HAY QUE GENERAR LAS CONSULTAS PARA LOS TUTORES
                                'TUTORES_FORMADORES' =>  array('TUTOR_FORMADOR' => $list2),
                                'USO' => 
                                        array(
                                            'HORARIO_MANANA' => 
                                                array(
                                                    'NUM_PARTICIPANTES' => $especialidad['NUM_PARTICIPANTES_M'], 
                                                    'NUMERO_ACCESOS' => $especialidad['NUMERO_ACCESOS_M'], 
                                                    'DURACION_TOTAL' => $especialidad['DURACION_TOTAL_M']
                                                ),
                                            'HORARIO_TARDE' => 
                                                array(
                                                    'NUM_PARTICIPANTES' => $especialidad['NUM_PARTICIPANTES_T'], 
                                                    'NUMERO_ACCESOS' => $especialidad['NUMERO_ACCESOS_T'], 
                                                    'DURACION_TOTAL' => $especialidad['DURACION_TOTAL_T']
                                                ),
                                            'HORARIO_NOCHE' => 
                                                array(
                                                    'NUM_PARTICIPANTES' => $especialidad['NUM_PARTICIPANTES_N'], 
                                                    'NUMERO_ACCESOS' => $especialidad['NUMERO_ACCESOS_N'], 
                                                    'DURACION_TOTAL' => $especialidad['DURACION_TOTAL_N']
                                                ),
                                            'SEGUIMIENTO_EVALUACION' => 
                                                array(
                                                    'NUM_PARTICIPANTES' => $especialidad['NUM_PARTICIPANTES_S'], 
                                                    'NUMERO_ACTIVIDADES_APRENDIZAJE' => $especialidad['NUMERO_ACTIVIDADES_APRENDIZAJE_S'], 
                                                    'NUMERO_INTENTOS' => $especialidad['NUMERO_INTENTOS_S'], 
                                                    'NUMERO_ACTIVIDADES_EVALUACION' => $especialidad['NUMERO_ACTIVIDADES_EVALUACION_S']
                                                )
                                        )
                    ); 

                    $list31 = array();
                    $participantes = $dbHandler->obtenerParticipantesAccion($id);
                    while ($participante = mysqli_fetch_assoc($participantes)){
                        $especialidadesParticipante = $dbHandler->obtenerEspecialidadParticipante($participante["ID_PARTICIPANTE"], $id);
                        while ($especialidadParticipante = mysqli_fetch_assoc($especialidadesParticipante)){
                            array_push($list31, 
                                array(
                                    'ID_ESPECIALIDAD' => 
                                        array(
                                            'ORIGEN_ESPECIALIDAD'  => $especialidadParticipante['ORIGEN_ESPECIALIDAD'], 
                                            'AREA_PROFESIONAL' => $especialidadParticipante['AREA_PROFESIONAL'], 
                                            'CODIGO_ESPECIALIDAD' => $especialidadParticipante['CODIGO_ESPECIALIDAD']
                                        ),
                                    'FECHA_ALTA'  => $especialidadParticipante['FECHA_ALTA'], 
                                    'FECHA_BAJA' => $especialidadParticipante['FECHA_BAJA'],
                                    'TUTORIAS_PRESENCIALES' => 
                                        array(
                                            'TUTORIA_PRESENCIAL' => 
                                                array (
                                                    'CENTRO_PRESENCIAL_TUTORIA' => 
                                                        array(
                                                            'ORIGEN_CENTRO'  => $especialidadParticipante['ORIGEN_CENTRO'], 
                                                            'CODIGO_CENTRO' => $especialidadParticipante['CODIGO_CENTRO']
                                                        ),
                                                    'FECHA_INICIO'  => $especialidadParticipante['FECHA_INICIO_T'], 
                                                    'FECHA_FIN' => $especialidadParticipante['FECHA_FIN_T']
                                                )
                                        ),
                                    'EVALUACION_FINAL' => 
                                        array(
                                            'CENTRO_PRESENCIAL_EVALUACION' => 
                                                array(
                                                    'ORIGEN_CENTRO'  => $especialidadParticipante['ORIGEN_CENTRO_EX'], 
                                                    'CODIGO_CENTRO' => $especialidadParticipante['CODIGO_CENTRO_EX']
                                                ),
                                            'FECHA_INICIO'  => $especialidadParticipante['FECHA_INICIO_EX'], 
                                            'FECHA_FIN' => $especialidadParticipante['FECHA_FIN_EX']
                                        ),
                                    'RESULTADOS' => 
                                        array(
                                            'RESULTADO_FINAL' => $especialidadParticipante['RESULTADO_FINAL'],
                                            'CALIFICACION_FINAL'  => $especialidadParticipante['CALIFICACION_FINAL'], 
                                            'PUNTUACION_FINAL' => $especialidadParticipante['PUNTUACION_FINAL']
                                        )
                                )
                            );

                        }

                        array_push($list3, 
                            array(
                                'ID_PARTICIPANTE' => 
                                    array(
                                        'TIPO_DOCUMENTO'  => $participante['TIPO_DOCUMENTO'], 
                                        'NUM_DOCUMENTO' => $participante['NUM_DOCUMENTO'], 
                                        'LETRA_NIF' => $participante['LETRA_NIF']
                                    ),
                                'INDICADOR_COMPETENCIAS_CLAVE'  => $participante['INDICADOR_COMPETENCIAS_CLAVE'],
                                'CONTRATO_FORMACION' => 
                                    array(
                                        'ID_CONTRATO_CFA' => $participante['ID_CONTRATO_CFA'],
                                        'CIF_EMPRESA' => $participante['CIF_EMPRESA'],
                                        'ID_TUTOR_EMPRESA' => 
                                            array(
                                                'TIPO_DOCUMENTO'  => $participante['TIPO_DOCUMENTO_TE'], 
                                                'NUM_DOCUMENTO' => $participante['NUM_DOCUMENTO_TE'], 
                                                'LETRA_NIF' => $participante['LETRA_NIF_TE']
                                            ),
                                        'ID_TUTOR_FORMACION' => 
                                            array(
                                                'TIPO_DOCUMENTO'  => $participante['TIPO_DOCUMENTO_TF'], 
                                                'NUM_DOCUMENTO' => $participante['NUM_DOCUMENTO_TF'], 
                                                'LETRA_NIF' => $participante['LETRA_NIF_TF']
                                            )
                                    ),
                                'ESPECIALIDADES_PARTICIPANTE' => array('ESPECIALIDAD' => $list31)
                            )
                        );
                    } 

                    return array(
                            'RESPUESTA_OBT_ACCION' => 
                                array(
                                    'CODIGO_RETORNO' => 0,
                                    'ETIQUETA_ERROR' => "Correcto", 
                                    'ACCION_FORMATIVA' => 
                                        array(
                                            'ID_ACCION' => 
                                                array(
                                                    'ORIGEN_ACCION' => $log['ACCION_FORMATIVA']['ORIGEN_ACCION'], 
                                                    'CODIGO_ACCION' => $log['ACCION_FORMATIVA']['CODIGO_ACCION']
                                                ),
                                            'SITUACION' => $log['ACCION_FORMATIVA']['SITUACION'],
                                            'ID_ESPECIALIDAD_PRINCIPAL' => 
                                                array(
                                                    'ORIGEN_ESPECIALIDAD' => $log['ACCION_FORMATIVA']['ORIGEN_ESPECIALIDAD'], 
                                                    'AREA_PROFESIONAL' => $log['ACCION_FORMATIVA']['AREA_PROFESIONAL'], 
                                                    'CODIGO_ESPECIALIDAD' => $log['ACCION_FORMATIVA']['CODIGO_ESPECIALIDAD']
                                                ),
                                            'DURACION' => $log['ACCION_FORMATIVA']['DURACION'],
                                            'FECHA_INICIO' => $log['ACCION_FORMATIVA']['FECHA_INICIO'],
                                            'FECHA_FIN' => $log['ACCION_FORMATIVA']['FECHA_FIN'],
                                            'IND_ITINERARIO_COMPLETO' => $log['ACCION_FORMATIVA']['IND_ITINERARIO_COMPLETO'],
                                            'TIPO_FINANCIACION' => $log['ACCION_FORMATIVA']['TIPO_FINANCIACION'],
                                            'NUMERO_ASISTENTES' => $log['ACCION_FORMATIVA']['NUMERO_ASISTENTES'],
                                            'DESCRIPCION_ACCION' => 
                                                array(
                                                    'DENOMINACION_ACCION' => $log['ACCION_FORMATIVA']['DENOMINACION_ACCION'], 
                                                    'INFORMACION_GENERAL' => $log['ACCION_FORMATIVA']['INFORMACION_GENERAL'], 
                                                    'HORARIOS' => $log['ACCION_FORMATIVA']['HORARIOS'], 
                                                    'REQUISITOS' => $log['ACCION_FORMATIVA']['REQUISITOS'], 
                                                    'CONTACTO_ACCION' => $log['ACCION_FORMATIVA']['CONTACTO_ACCION']
                                                ),
                                            'ESPECIALIDADES_ACCION' => array('ESPECIALIDAD' => $list),
                                            'PARTICIPANTES' => array ('PARTICIPANTE' => $list3)
                                        )
                                )
                    );
                }
            } elseif($log['CODIGO_RETORNO'] == 1) {
                return array('RESPUESTA_OBT_ACCION' => array( 

                            'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],

                            'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'] 

                ));        
            }
        } else {
            return array(
                    'RESPUESTA_OBT_ACCION' => 
                        array( 
                            'CODIGO_RETORNO' => 2,
                            'ETIQUETA_ERROR' => 'Error en parámetro de entrada'
                        )
            );           
        }
    } else {
        return array(
                'RESPUESTA_OBT_ACCION' => 
                    array( 
                        'CODIGO_RETORNO' => -1,
                        'ETIQUETA_ERROR' => 'Error inesperado: Error de logeo',
                    )
        );         
    }
}

/**
 * Elimino las acciones formativas
 *
 * @return array 
 */
function eliminarAccion($accion) {

    $dbHandler = new DbHandler();

    $authenticate = $dbHandler->authenticate();

    if($authenticate){
        $log = $dbHandler->eliminarAccion($accion);

        return array(
            'RESPUESTA_ELIMINAR_ACCION' => 
                array(
                    'CODIGO_RETORNO' => $log['CODIGO_RETORNO'],
                    'ETIQUETA_ERROR' => $log['ETIQUETA_ERROR'],
                )
        );

    } else {
        return array(
            'RESPUESTA_ELIMINAR_ACCION' => 
                array(
                    'CODIGO_RETORNO' => -1,
                    'ETIQUETA_ERROR' => 'Error inesperado: Error de logeo',
                )
        );        
    }
}

$server->AddFunction("crearCentro");
$server->AddFunction("obtenerDatosCentro");
$server->AddFunction("crearAccion");
$server->AddFunction("obtenerListaAcciones");
$server->AddFunction("obtenerAccion");
$server->AddFunction("eliminarAccion");

$server->handle();
